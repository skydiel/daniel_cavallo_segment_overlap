# SEGMENT OVERLAP

A standalone program to detect if two segments overlap.

## Usage

This program requires Python 3. No libraries are needed.

```
$ python3 segment_overlap.py

Enter segment 0 points (i,j): (0,10)
Enter segment 0 points (i,j): (4,8)
Segment (0, 10) and (4, 8) overlap on (4, 8)
```
_Note that when entering the point, you must use the parentheses._
