import sys
import re


def check_segment_overlap(segment1, segment2):
    """Check if two segments overlap.

    :param segment1: a tuple with two integers where the left integer is less than or equal the right one.
    :param segment2: idem segment1

    :return: a tuple with two integers indicating where the two given segments overlap, or None
             if there is no overlap.
    """
    # garantee that s1 is on the left side of s2
    s1, s2 = (segment1, segment2) if segment1 < segment2 else (segment2, segment1)

    return (max(s1[0], s2[0]), min(s1[1], s2[1])) if s1[1] > s2[0] else None


def main():
    pattern = re.compile(r'^\((\d+),\s*(\d+)\)')
    points = []
    for i in range(2):
        print('Enter segment {} points (i,j): '.format(i), end='')
        match = pattern.match(input())
        if not match:
            print('ERROR: Input line bad format. It must be (i,j) where i and j are integers.')
            exit(1)

        point = (int(match.group(1)), int(match.group(2)))

        if (point[0] > point[1]):
            print('ERROR: Point (i={},j={}) is inconsistent: i must be greater than j.'.format(point[0], point[1]))
            exit(1)

        points.append(point)

    overlap = check_segment_overlap(points[0], points[1])

    if overlap:
        print('Segment {} and {} overlap on {}'.format(points[0], points[1], overlap))
    else:
        print('Segment {} and {} do not overlap.'.format(points[0], points[1]))


if __name__ == "__main__":
    main()
